#include <emscripten.h>
#include <emscripten/bind.h>
#include <emscripten/html5_webgpu.h>
#include <emscripten/bind.h>
#include <future>
#include <iostream>
#include <stdexcept>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>


using namespace emscripten;
using namespace std::chrono_literals;

int test() {
  int a = 78;
  std::future<int> f2 = std::async(std::launch::async, [&a]{
    for(int i = 0; i < 100000000; ++i){ 
      // std::cout << a;
    }
    std::this_thread::sleep_for(5000ms);
    return 8;
  });
  std::future_status status;
  do {
      status = f2.wait_for(0s);
      switch(status) {
          case std::future_status::deferred: std::cout << "deferred\n"; break;
          case std::future_status::timeout: std::cout << "timeout\n"; break;
          case std::future_status::ready: std::cout << "ready!\n"; break;
      }
      a++;
      emscripten_sleep(100);
  } while (status != std::future_status::ready);
  std::cout << "test!\n";
  return f2.get();
  return 8;
}

EMSCRIPTEN_BINDINGS(my_module) { 
  function("test", &test); 
}
